#!/src/ucbl_space_filtering/
"""! @brief Main program for the space filtering module."""

##
# @file doxygen_example.py
#
# @brief Main program for the space filtering module.
#
#
# @section libraries_main Libraries/Modules
# - OpenCV : OpenCV is a highly optimized library with focus on real-time applications.
#
# @section todo_doxygen_example TODO
# - None.
#
# @section author_doxygen_example Author(s)
# - Created by Axel Paccalin on 26/05/2021.
# - Modified by Boeglin Cyril on 27/05/2021.
#
# Copyright (c) 2021  Axel Paccalin & Boeglin Cyril.  All rights reserved.

# Imports
import sys, getopt
import functools


from ucbl_video_io import VideoReader, VideoWriter
from ucbl_space_filtering.Filter import factory


def usage():
    print("Usage: \n -i, --input     select a file as input (image or video). \n -o, --output\
    the recipient file. \n And, specified the filter you want in quotation marks. \n Filters : \n\
      Average(dimension)\n      Convolution(dimension)\n      Median(dimension)\n      Gaussian(dimension,*optional* sigma)\n\n\
example : python3 -m ucbl_space_filtering -i myVideo.mp4 -o output.mp4 \"Average(3)\"" )


def read_args():
    """! Recover the arguments write in the command line.
    @return  The provided arguments.
    """
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hg:i:o:d", ["help", "input=", "output="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    input_file_arg = None
    output_file_arg = None

    # Identify the different arguments recover from the command line to sort them
    for op, arg in opts:
        if op in ("-h", "--help"):
            usage()
            sys.exit()
        elif op == '-d':
            global _debug
            _debug = 1
        elif op in ("-i", "--input"):
            input_file_arg = arg
        elif op in ("-o", "--output"):
            output_file_arg = arg

    return input_file_arg, output_file_arg, args


def main():
    """! Main program entry."""
    if_arg, of_arg, filters_arg = read_args()
    # Print the different args to possibly verify them
    print(if_arg)
    print(of_arg)
    print(filters_arg)

    # Check if the if & of arguments are present because they are mandatory
    if if_arg is None:
        raise AttributeError("Missing input (-i <input path>)/(--input=<input path>) argument")
    if of_arg is None:
        raise AttributeError("Missing output (-o <output path>)/(--output=<output path>) argument")

    filters = []
    for arg in filters_arg:
        try:
            filters.append(factory(arg))
        except:
            raise AttributeError("Unexpected error while parsing filter \"" + arg + "\"\n" + sys.exc_info()[0])
            
    with VideoReader(if_arg) as input_file:
        with VideoWriter(of_arg, input_file.get_meta()) as output_file:
            for frame in input_file.read():
                output_file.write(functools.reduce(lambda fr, fil: fil.apply(fr), filters, frame))


if __name__ == "__main__":
    # execute only if run as a script
    main()
