import cv2


class Convolution(object):
    """! The Convolution class.
        Defines the convolution function utilized.
        """
    def __init__(self, kernel):
        """! The Convolution base class initializer.
                @param kernel  The kernel use for the function.
                @return  An instance of the Convolution class initialized with the specified kernel.
                """
        # The kernel of the Convolution class
        self.kernel = kernel

    def apply(self, frame):
        """! The Convolution class application.
                        @param frame  Image were the Convolution function is apply.
                        @return  The results of the function apply to this frame (of a video for example).
                        """
        return cv2.filter2D(frame, -1, self.kernel)


class Average(object):
    """! The Average class.
        Defines the average function utilized.
        """
    def __init__(self, dim):
        """! The Average base class initializer.
                        @param dim  The dimension use for the function.
                        @return  An instance of the Average class initialized with the specified kernel.
                        """
        # The dimension of the Average class
        self.dim = dim

    def apply(self, frame):
        """! The Average class application.
                                @param frame  Image were the Average function is apply.
                                @return  The results of the function apply to this frame (of a video for example).
                                """
        return cv2.blur(frame, (self.dim, self.dim))


class Median(object):
    """! The Average class.
            Defines the average function utilized.
            """
    def __init__(self, dim):
        """! The Average base class initializer.
                                @param dim  The dimension use for the function.
                                @return  An instance of the Average class initialized with the specified dimension.
                                """
        # The dimension of the Average class
        self.dim = dim

    def apply(self, frame):
        """! The Average class application.
                                        @param frame  Image were the Average function is apply.
                                        @return  The results of the function apply to this frame (of a video for example).
                                        """
        return cv2.medianBlur(frame, self.dim)


class Gaussian(object):
    """! The Gaussian class.
                Defines the gaussian function utilized.
                """
    def __init__(self, dim, sig=1):
        """! The Gaussian base class initializer.
                                        @param dim  The dimension use for the function.
                                        @param sig Signal use for the process of the function
                                        @return  An instance of the Gaussian class initialized with the specified dimension.
                                        """
        self.dim = dim
        self.sig = sig

    def apply(self, frame):
        """! The Gaussian class application.
                                        @param frame  Image were the Gaussian function is apply.
                                        @return  The results of the function apply to this frame (of a video for example).
                                        """
        return cv2.GaussianBlur(frame, (self.dim, self.dim), self.sig, self.sig)


def factory(definition):
    """! The factory definition.
                    Defines the factory for using the previous function.
                    """
    return eval(definition)
