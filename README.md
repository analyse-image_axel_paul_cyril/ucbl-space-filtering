# UCBL-STATIC-FILTERING PACKAGE

In this package, you will find:

- The Filter Filter.py file contains definition for various space filtering algorithms (ex : Average, Median,etc...).
- The __main__.py file is the entry point of the program. It parses command arguments, and [load => filter => write] a video file accordingly.

Accordingly to this package, we provides somes usage of this package :

`python3 -m ucbl_space_filtering -i path/to/input.mp4 -o path/to/output.mp4 "Gaussian(5, 2)" (gaussian filter, kernel=(5x5), sigma=2)`

This command lauch the filtering of an video locate on "path/to/output" with the Gaussian function with a 5x5 kernel.
We can have anothers examples:

`python3 -m ucbl_space_filtering -i path/to/input.mp4 -o path/to/output.mp4 "Average(3)" (average filter, kernel=(3x3))`

`python3 -m ucbl_space_filtering -i path/to/input.mp4 -o path/to/output.mp4 "Median(7)" (median filter, kernel=(7x7))`


